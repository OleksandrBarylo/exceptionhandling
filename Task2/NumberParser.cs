﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Task2
{
    public class NumberParser : INumberParser
    {
        public int Parse(string stringValue)
        {
            ThrowIfNullOrEmptyInput(stringValue);

            stringValue = stringValue.Trim();

            int val = 0;
            int charge = 0;

            bool isSigned = stringValue[0] == '-';
            int endIndex = stringValue[0] == '-' || stringValue[0] == '+' ? 1 : 0;

            for (int i = stringValue.Length - 1; i >= endIndex; i--)
            {
                checked
                {
                    val += GetChargeIncrement(stringValue[i], ref charge, isSigned);
                }
            }

            return val;
        }

        private int GetChargeIncrement(char ch, ref int charge, bool isSigned)
        {
            if (ch == ' ') return 0;

            int increment = GetCharToDigitEquivalent(ch) * (int) Math.Pow(10, charge++);

            return isSigned ? -increment : increment;
        }

        private int GetCharToDigitEquivalent(char ch) =>
            ch switch
            {
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '0' => 0,
                _ => throw new FormatException($"Invalid char '{ch}' in input string.")
            };

        private static void ThrowIfNullOrEmptyInput(string stringValue)
        {
            if (stringValue == null)
            {
                throw new ArgumentNullException(nameof(stringValue));
            }

            if (string.IsNullOrWhiteSpace(stringValue))
            {
                throw new FormatException(nameof(stringValue));
            }
        }
    }
}