﻿using System;
using System.Threading.Channels;

namespace Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Enter line and the application will print the first letter.");
            Console.WriteLine("Press any button to start...");

            Console.ReadKey(true);

            while (true)
            {
                Console.WriteLine($"\n{GetFirstInputChar()}");
            }
        }

        private static char GetFirstInputChar()
        {
            var input = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("You entered an empty line! Try again.");

                GetFirstInputChar();
            }

            return input[0];
        }
    }
}